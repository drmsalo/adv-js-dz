"use strict";
// Каждый обьект имеет внутреннюю ссылку на другой обьект, называемый его прототипом.
// super() используется как функция вызывающая родительский конструктор, что бы использовать ключевое слово this

class Employee {
  constructor(name, age, salary) {
    (this.name = name), (this.age = age), (this.salary = salary);
  }
  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }

  set name(newName) {
    return (this._name = newName);
  }
  set age(newAge) {
    return (this._age = newAge);
  }
  set salary(newSalary) {
    return (this._salary = newSalary);
  }
}
class Programmer extends Employee {
  constructor(lang, name, age, salary) {
    super();
    this.name = name;
    this.age = age;
    this.salary = salary * 3;
    this.lang = lang;
  }
  get name() {
    return super.name;
  }
  get age() {
    return super.age;
  }
  get salary() {
    return super.salary * 3;
  }

  set name(newName) {
    return (super.name = newName);
  }
  set age(newAge) {
    return (super.age = newAge);
  }
  set salary(newSalary) {
    return (super.salary = newSalary);
  }
}
const john = new Programmer("js", "john", 23, 3000);
const jack = new Programmer("React", "Jack", 28, 4000);
const shawn = new Programmer("C#", "Shawn", 25, 3500);
console.log(john);
console.log(jack);
console.log(shawn);
