const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const listContainer = document.querySelector("#root");

class WithoutProp extends Error {
  constructor(value, index) {
    super();
    this.name = `Missing property`;
    this.message = `In book with number ${index} missing property - "${value.toUpperCase()}"`;
  }
}

books.forEach((elem, index) => {
  let numOfBook = index + 1;
  try {
    if (Object.keys(elem).length < 3) {
      throw new WithoutProp();
    } else {
      listContainer.insertAdjacentHTML(
        "beforeend",
        `<ul class="list"><li class="title">Book ${numOfBook}</li></ul>`
      );
      for (const key in elem) {
        document
          .querySelectorAll("ul")
          [document.querySelectorAll("ul").length - 1].insertAdjacentHTML(
            "beforeend",
            `<li>${key.charAt(0).toUpperCase() + key.slice(1)} - ${
              elem[key]
            }</li>`
          );
      }
    }
  } catch {
    if (!elem.hasOwnProperty("author")) {
      console.log(new WithoutProp("author", numOfBook));
    } else if (!elem.hasOwnProperty("name")) {
      console.log(new WithoutProp("name", numOfBook));
    } else if (!elem.hasOwnProperty("price")) {
      console.log(new WithoutProp("price", numOfBook));
    }
  }
});
