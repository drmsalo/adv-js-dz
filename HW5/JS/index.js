"use strict";
const postsContainer = document.querySelector(".posts__container");
class Post {
  constructor(body, title, userId, name, mail, id) {
    (this.body = body),
      (this.title = title),
      (this.userId = userId),
      (this.name = name),
      (this.mail = mail),
      (this.id = id),
      (this.deleteButton = document.createElement("button"));
  }

  render() {
    postsContainer.insertAdjacentHTML(
      "afterbegin",

      `<div class="post post-${this.id}">
  
        
              <h2 class="post__title">${this.title}</h2>
  
              <button class="delete">DELETE</button>
  
              <p class="post__info">${this.body}</p>
  
  
              <div class="post__user">
  
                  <h3 class="user__name">${this.name}</h3>
  
                  <a href="${this.mail}" class="user__mail">${this.mail}</a>
  
               </div>
  
  
         </div>`
    );

    document.querySelector(".delete").addEventListener("click", (ev) => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
        method: "DELETE",
      }).then(() => {
        postsContainer.removeChild(document.querySelector(`.post-${this.id}`));
      });
    });
  }
}

fetch("https://ajax.test-danit.com/api/json/posts")
  .then((response) => response.json())

  .then((r) =>
    r.forEach(({ body, title, userId, id }) =>
      fetch("https://ajax.test-danit.com/api/json/users")
        .then((re) => re.json())

        .then((res) =>
          res.forEach(({ name, email, id: currentUserId }) => {
            if (currentUserId === userId) {
              new Post(body, title, userId, name, email, id).render();
            }
          })
        )

        .catch((err) => console.warn(err))
    )
  )

  .catch((err) => console.warn(err));
