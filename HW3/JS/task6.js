"use strict";

const employee = {
  name: "Vitalii",
  surname: "Klichko",
};

const fullEmployee = { ...employee, age: 30, salary: 4500 };
console.log(fullEmployee);
