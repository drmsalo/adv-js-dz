"use strict";

const array = ["value", () => "showValue"];

const [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue()); // должно быть выведено 'showValue'
