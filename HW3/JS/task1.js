"use strict";

const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const checkArr = (arrOne, arrTwo) => {
  const newArr = arrOne.filter((elem) => {
    if (!arrTwo.includes(elem)) {
      return elem;
    }
  });
  return newArr;
};

const clients = [...clients1, ...checkArr(clients2, clients1)];
console.log(clients);
