"use strict";
const url = "https://ajax.test-danit.com/api/swapi/films";
const listofCharacters = document.querySelector(".characters__list");

fetch(url)
  .then((response) => response.json())
  .then((films) => {
    films.forEach(({ episodeId, name, openingCrawl, characters }) => {
      document.querySelector(".container").insertAdjacentHTML(
        "afterbegin",
        `<h1>Episode: ${episodeId}</h1>
        <h2>${name}</h2>
      <p>${openingCrawl}</p>
      <div class = "film-special-card film-card${episodeId}"></div>
      <ul class = "characters${episodeId}"></ul>
      
      
      `
      );
      const roles = characters.map((el) => fetch(el).then((res) => res.json()));
      Promise.allSettled(roles).then((data) => {
        data.forEach((elem) => {
          const {
            value: { name },
          } = elem;
          // if (elem.status === "fulfilled") {
          document
            .querySelector(`.characters${episodeId}`)
            .insertAdjacentHTML("beforeend", `<li>${name}</li>`);
          // }
        });
      });
    });
  });
